FROM alpine:3.10

# install package i need
RUN apk add --update bash mysql-client gzip openssl && rm -rf /var/cache/apk/*

ENV CRON_TIME="0 1 * * mon" \
    MYSQL_USER="user" \
    MYSQL_PASSWORD="password" \
    MYSQL_DATABASE="database_name" \
    MYSQL_HOST="mysql" \
    MYSQL_PORT="3306"

# copy stripts to container
COPY ./run.sh /
COPY ./backup.sh /

RUN chmod u+x /backup.sh /run.sh

# create and setup basic folders && access rights
RUN mkdir /backup_data
RUN mkdir /backup_log

# creating volume
VOLUME ["/backup_data"]

# run command
CMD /run.sh