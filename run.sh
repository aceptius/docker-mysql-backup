#!/bin/bash
echo "Creating log file ..."
touch /backup_log/backup.log

echo "Creating crontab configuration ..."
echo "${CRON_TIME} /backup.sh >> /backup_log/backup.log 2>&1" > /crontab.conf
echo "Configuration:" && cat /crontab.conf

echo "Starting backup cron ... "
crontab /crontab.conf
exec crond -f