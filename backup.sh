#!/bin/sh

[ -z "${MYSQL_USER}" ] && { echo "=> MYSQL_USER cannot be empty" && exit 1; }
[ -z "${MYSQL_PASSWORD}" ] && { echo "=> MYSQL_PASSWORD cannot be empty" && exit 1; }
[ -z "${MYSQL_DATABASE}" ] && { echo "=> MYSQL_DATABASE cannot be empty" && exit 1; }
[ -z "${MYSQL_HOST}" ] && { echo "=> MYSQL_DATABASE cannot be empty" && exit 1; }


FILE_DATE=$(date +%Y-%m-%d-%H-%M-%S)
echo "=> Backup started at $(date "+%Y-%m-%d %H:%M:%S")"


FILENAME=/backup_data/backup-$MYSQL_DATABASE-$FILE_DATE.sql

if mysqldump -h "$MYSQL_HOST" -P "$MYSQL_PORT" -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE" > "$FILENAME"
    then
        gzip "$FILENAME"
else
    echo "Error creating backup"
fi
